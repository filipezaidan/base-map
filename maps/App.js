import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import MapView from 'react-native-maps';

export default class App extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null
    };
  }

  async componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }
  
  render() {
    return (
      <View style={styles.container}>
        <MapView style={styles.map}
          region={{
            latitude:this.state.latitude ? this.state.latitude :40.6974034,
            longitude:this.state.longitude ? this.state.longitude : -74.1197632,
            latitudeDelta: 0.1,
            longitudeDelta: 0.1,
          }}
        >

          <MapView.Marker
            coordinate={{
              latitude: this.state.latitude ? this.state.latitude : 40.6974034 ,
              longitude: this.state.longitude ? this.state.longitude : -74.1197632  
            }}
            title="Ponta verde !!!!"
            description="HRHHRHRHRR"
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top:0,
    left:0,
    bottom:0,
    right:0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  map:{
    position: 'absolute',
    top:0,
    left:0,
    bottom:0,
    right:0,
  },
});
